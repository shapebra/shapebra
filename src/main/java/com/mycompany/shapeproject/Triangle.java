/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.shapeproject;

/**
 *
 * @author BankMMT
 */
public class Triangle {
    private double h;
    private double base;
    public static final double recipe = 0.5;
    public Triangle(double h,double base){
        this.h = h ;
        this.base = base;
    }public double calArea(){
        double result = recipe*h*base;
        return result;
    }public double getH(){
        return h;
    }public double getBase(){
        return base;
    }public void setH(double h){
        if (h <= 0){
            System.out.println("Error: Height must more than zero !!!");
            return;
        }this.h = h;
    }public void setBase(double base){
        if (base <= 0){
            System.out.println("Error: Base must more than zero !!!");
            return;
        }this.base = base;
    }
}
