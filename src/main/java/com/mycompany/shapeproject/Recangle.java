/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.shapeproject;

/**
 *
 * @author BankMMT
 */
public class Recangle {
    private double w;
    private double l;
    public Recangle(double w,double l){
        this.w = w ;
        this.l = l;
    }public double calArea(){
        double result = w*l;
        return result;
    }public double getWidth(){
        return w;
    }public double getLength(){
        return l;
    }public void setWidth(double w){
        if (w <= 0){
            System.out.println("Error: Width must more than zero !!!");
            return;
        }this.w = w;
    }public void setLength(double l){
        if (l <= 0){
            System.out.println("Error: Length must more than zero !!!");
            return;
        }this.l = l;
    }
}
