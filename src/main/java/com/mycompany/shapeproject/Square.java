/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.shapeproject;

/**
 *
 * @author BankMMT
 */
public class Square {
    private double e;                   // e = edge
    public Square (double e){
        this.e = e;
    }public double calArea(){
        double result = e*e;
        return result;
    }public double getEdge(){
        return e;
    }public void setEdge(double e){
        if (e <= 0){
            System.out.println("Error: Edge must more than zero !!!");
            return;
        }this.e = e;
    }
}
