/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.shapeproject;

/**
 *
 * @author BankMMT
 */
public class Circle {
    private double r;
    public static final double pi = 22.0/7;
    public Circle (double r){
        this.r = r ;
    }public double calArea(){
        double result = pi*(r*r);
        return result;
    }public double getR(){
        return r;
    }public void setR(double r){
        if (r <= 0){
            System.out.println("Error: Radius must more than zero !!!");
            return;
        }this.r = r;
    }
        
    
}
